var express = require('express');
var router = express.Router();
var hive = require('@hiveio/hive-js');
const config = require('../config')
var jwt = require('jsonwebtoken');

/* GET home page. */
router.get('/completeauth', function(req, res) {
  if (req.query.access_token) {
    const accessToken = req.query.access_token;
    const username = req.query.username;
    const expiresIn = req.query.expires_in;

    res.render('spinner', {
        auth: true,
        token: accessToken,
        username: username
    })

  }
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { loggedIn: false });
});

router.get('/:username/tasks', function(req, res) {
  res.render('index', {
    username: req.params.username, 
    loggedIn: true 
  });
})

router.get('/:username/create', function(req, res) {
  res.render('create', {
    username: req.params.username, 
    loggedIn: true 
  });
})


router.get('/:username/my', function(req, res) {
  let username, loggedIn, currentProfile;

  username = req.query.username;
  currentProfile = req.query.currentProfile
  loggedIn = req.query.loggedIn;

  if (loggedIn === 'true') {
    loggedIn = true
  }

  if (loggedIn === 'false') {
    loggedIn = false
  }

  res.render('profile', {
    username,
    loggedIn,
    currentProfile
  });
})

router.get('/task/:permlink', async (req, res) => {
  let username, loggedIn, currentProfile;

  username = req.query.username;
  currentProfile = req.query.currentProfile
  loggedIn = req.query.loggedIn;

  if (loggedIn === 'true') {
    loggedIn = true
  }

  if (loggedIn === 'false' || !loggedIn) {
    loggedIn = false
  }

  if (!username) {
    username = null;
  }

  res.render('task', {
    username,
    loggedIn,
    currentProfile
  });
})

/*router.get('/:author/:permlink', async (req, res) => {
  let username, loggedIn, currentProfile;

  username = req.query.username;
  currentProfile = req.params.author
  loggedIn = req.query.loggedIn;

  if (loggedIn === 'true') {
    loggedIn = true
  }

  if (loggedIn === 'false' || !loggedIn) {
    loggedIn = false
  }

  if (!username) {
    username = null;
  }

  res.render('task', {
    username,
    loggedIn,
    currentProfile
  });
})*/

router.get('/:username/bids/all', function(req, res) {
  const username = req.params.username;
  let loggedIn = req.query.loggedIn;
  

  if (loggedIn === 'true') {
    loggedIn = true
  }

  if (loggedIn === 'false' || !loggedIn) {
    loggedIn = false
  }
  res.render('bids', {
    username,
    loggedIn
  });
})

router.get('/success', function(req, res) {
  const username = req.query.username;
  let loggedIn = req.query.loggedIn;
  

  if (loggedIn === 'true') {
    loggedIn = true
  }

  if (loggedIn === 'false' || !loggedIn) {
    loggedIn = false
  }
  res.render('created', {
    username,
    loggedIn
  });
})


module.exports = router;

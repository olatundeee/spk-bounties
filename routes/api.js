var express = require('express');
var router = express.Router();
var hive = require('@hiveio/hive-js');
const config = require('../config')
var jwt = require('jsonwebtoken');

const taskModel = require('../models/task')
const currentModel = require('../models/current')


router.post('/get_hive_account_info', async (req, res) => {
    const username = req.body.username

    hive.api.getAccounts([username], function(err, result) {
        if (err) {
            console.log(err)
        }

        if (result) {
            return res.json(result)
        }
    });
    
    
})

router.post('/keychain/fetch_memo', async (req, res) => {
    const username = req.body.username

    if (username && username.length < 16 && username.length > 3) {
        let data = await hive.api.getAccountsAsync([username]);
        let pub_key = data[0].posting.key_auths[0][0];

        if (data.length === 1)
        {
            const speakBountiesWif = '5Hqg424NMKGe8PtkzmhCc5no2cCRYJCPq6b7YQwTJ28mj3wKYgx'

            var encoded = await hive.memo.encode(speakBountiesWif, pub_key, `log user in`);

            return res.json({
                username,
                encoded
            });
        }
    }
    
})

router.post('/keychain/callback', async (req, res) => {
    const username = req.body.theUsername
    const successMessage = req.body.successMessage;

    if (successMessage) {
        // create token and store in token variable

        var token = jwt.sign({ id: username }, config.secret, {
            expiresIn: 86400
        });

        return res.json({
            auth: true,
            token: token,
            username: username
        });
    }
    
})

router.post('/posting/login', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    hive.api.getAccounts([username], function(err, user){

        // store postinq wif in variable

        const pubWif = user[0].posting.key_auths[0][0];

        // check for the validity of the posting key

        if(hive.auth.isWif(password)){
            // check if the public key tallies with the private key provided

            const Valid = hive.auth.wifIsValid(password, pubWif);

            if(Valid){
               // create token and store in token variable

                var token = jwt.sign({ id: user._id }, config.secret, {
                    expiresIn: 86400
                });

                // if user authentication is successful send auth confirmation, token and user data as response

                res.json({
                    auth: true,
                    token: token,
                    username: user[0].name
                });
            } else {
                return res.json({
                    auth: false,
                    token: null,
                    user: null
                })
            }
        } else {
            return res.json({
                auth: false,
                token: null,
                user: null
            })
        }
    });
    
})

router.get('/logout', function(req, res){
    // send response to falsify authentication and nullify token
  
    res.json({ 
      auth: false,
      token: null
    })
})

router.post('/create', async (req, res) => {
    const body = req.body;

    const hive = req.body.comment
    const finalDataObj = req.body.finalDataObj

    try {
        
        const saveTask = await taskModel.create({
            hive,
            finalDataObj,
            bids: []
        })

        return res.json({message: 'success'})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.get('/all', async (req, res) => {
    try {
        let getTasks = await taskModel.find({});
        /*
        *
        * Don't remove this 
        getTasks = getTasks.forEach(async (task) => {
            await taskModel.findByIdAndDelete(task.id)
        })
        * 
        * 
        ****/
        return res.json({tasks: getTasks})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/yours', async (req, res) => {
    try {
        const author = req.body.username;
        let getTasks = await taskModel.find({
            'hive.author': author
        });
        /*
        *
        * Don't remove this 
        getTasks = getTasks.forEach(async (task) => {
            await taskModel.findByIdAndDelete(task.id)
        })
        * 
        * 
        ****/
        return res.json({tasks: getTasks})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/my', async (req, res) => {
    try {
        const getTasks = await taskModel.find({});

        return res.json({tasks: getTasks})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/task', async (req, res) => {
    try {
        const getTask = await taskModel.findOne({
            'hive.author': req.body.author,
            'hive.permlink': req.body.permlink
        });

        return res.json({task: getTask})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/add/bid', async (req, res) => {
    try {
        const permlink = req.body.taskPermlink
        const author = req.body.comment.parent_author
        const priceMilestone = req.body.priceMilestone
        const priceLump = req.body.priceLump
        const bidComment = req.body.bidComment;
        const bidPrevExp = req.body.bidPrevExp

        let getTask = await taskModel.findOne({
            'hive.author': author,
            'hive.permlink': permlink
        });

        const bid = req.body

        let theBids = getTask.bids

        theBids.push({hive: req.body.comment, otherData: {priceMilestone, priceLump, taskPermlink: permlink, bidComment, bidPrevExp}});

        //theBids = getTask.bids = [];

        const getNewTask = await taskModel.findByIdAndUpdate(getTask._id, {bids: theBids});

        return res.json({message: 'success'});
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/current', async (req, res) => {
    const body = req.body;
    const finalDataObj = req.body.current.finalDataObj;
    const author = req.body.current.author;

    try {
        
        const getTask = await currentModel.findOne({author})

        if (getTask) {
            const updateTask = await currentModel.findOneAndUpdate({author}, req.body.current)
            return res.json({message: 'updated', updateTask})
        }else {
            const createTask = await currentModel.create(req.body.current)
            return res.json({message: 'created', createTask})
        }

 
        /*
        *
        * Don't remove this 
        let getTasks = await currentModel.find();
        getTasks = getTasks.forEach(async (task) => {
            await currentModel.findByIdAndDelete(task.id)
            console.log('console.log(getTask)')
        })
        * 
        * 
        ****/
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.get('/current/:author', async (req, res) => {
    const author = req.params.author;

    try {
        
        const getTask = await currentModel.findOne({author})
        return res.json({task: getTask})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/select/bid', async(req, res) => {
    let getTask = await taskModel.findOne({
        'hive.author': req.body.taskAuthor,
        'hive.permlink': req.body.taskPermlink
    });

    function search(author, permlink, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].hive.author === author && myArray[i].hive.permlink === permlink) {
                return {bid: myArray[i], i};
            }
        }
    }
    let theBid
    
    if (getTask.finalDataObj.taskFunding === 'SPK Network DAO(Open Source)') {
        await getTask.bids.forEach(bid => {
            let voteValSupport = 0;
            bid.votes.forEach(vote => {
                if (vote.voteType === 'support') {
                    voteValSupport += parseFloat(vote.voterHivePower)
                }
            })

            if (req.body.voteValSupport == voteValSupport && req.body.author === bid.hive.author && req.body.permlink === bid.hive.permlink) {
                theBid = {bid, i: getTask.bids.indexOf(bid)}
            };
        })
    } else {
        theBid = search(req.body.author, req.body.permlink, getTask.bids)
    }

    try {
        console.log('getNewTask.bids[theBid.i]')

        if(theBid) {
            console.log('getNewTask.bids[theBid.i] 2')
            theBid.bid.otherData.selected = true
        }
        getTask.bids[theBid.i] = theBid.bid
        getTask.finalDataObj.selectionStatus = true

        console.log('getNewTask.bids[theBid.i] 3')
        const getNewTask = await taskModel.findByIdAndUpdate(getTask._id, getTask);

        console.log('getNewTask.bids[theBid.i] 4')
        console.log(getNewTask.bids[theBid.i])

        if (getNewTask.bids[theBid.i].otherData.selected === true && getNewTask.finalDataObj.selectionStatus === true) {
                console.log('success')
                return res.json({message: 'success'});
        } else {
                console.log('failed')
                return res.json({message: 'failed'});
        }
    } catch (error) {
        return res.json({message: error.message});
    }
    
})

router.post('/current/:author', async (req, res) => {
    const author = req.params.author;

    try {
        
        const getTask = await currentModel.findOne({author})

        await currentModel.findByIdAndDelete(getTask.id)
        return res.json({message: 'deleted'})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/vote/add', async (req, res) => {
    const author = req.body.voteReceiver;
    const permlink = req.body.permlink;

    try {
        
        let getTask = await taskModel.findOne({
            'hive.author': author,
            'hive.permlink': permlink
        });

        let theVotes = getTask.votes

        let message = ''

        await theVotes.forEach(async (voteOne) => {
            if (voteOne.voter === req.body.voter) {
                message = 'duplicate vote found'
            }
        })
        if (message !== 'duplicate vote found') {
            await theVotes.push(req.body);
            const getNewTask = await taskModel.findByIdAndUpdate(getTask._id, {votes: theVotes});

            return res.json({message: 'success'});
        } else {
            return res.json({message});
        }

        /**/
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})


router.post('/vote/bid', async (req, res) => {
    const author = req.body.voteTaskAuthor;
    const permlink = req.body.voteTaskPermlink;

   try {
        
        let getTask = await taskModel.findOne({
            'hive.author': author,
            'hive.permlink': permlink
        });
        
        let theBids = getTask.bids
        
        await theBids.forEach(async bid => {
            if(bid.hive.author === req.body.voteBidAuthor && bid.hive.permlink === req.body.voteBidPermlink)
            {
                let theVotes = bid.votes;

                let message = ''

                await theVotes.forEach(async (voteOne) => {
                    if (voteOne.voter === req.body.voter) {
                        message = 'duplicate vote found'
                    }
                })
                if (message !== 'duplicate vote found') {
                    await theVotes.push(req.body);
                    getTask.bids[theBids.indexOf(bid)] = bid
                    console.log(getTask.bids[theBids.indexOf(bid)])
                    const getNewTask = await taskModel.findByIdAndUpdate(getTask._id, getTask);

                    return res.json({message: 'success', getNewTask});
                } else {
                    return res.json({message});
                }
            }
        })

        
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.get('/all/bids', async (req, res) => {
    let allBids = []
    try {
        let getTasks = await taskModel.find({});

        await getTasks.forEach(async one => {
            if (one.bids.length > 0) {
                await one.bids.forEach(bid => {
                    let bidInfo =  {
                        hive: bid.hive,
                        otherData: bid.otherData,
                        votes: bid.votes,
                        paymentMode: one.finalDataObj.paymentMode,taskFunding: one.finalDataObj.taskFunding,
                        selectedStatus: one.finalDataObj.selectionStatus,
                        proposalStatus: one.finalDataObj.proposalStatus,
                        taskTitle: one.finalDataObj.taskTitle
                    }
                    allBids.push(bidInfo);
                })
            }
        })
        return res.json({tasks: allBids})

    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/bids/yours', async (req, res) => {
    let allBids = []
    try {
        let getTasks = await taskModel.find({});

        await getTasks.forEach(async one => {
            if (one.bids.length > 0) {
                await one.bids.forEach(bid => {
                    if (bid.hive.author === req.body.username) {
                        let bidInfo =  {
                            hive: bid.hive,
                            otherData: bid.otherData,
                            votes: bid.votes,
                            paymentMode: one.finalDataObj.paymentMode,taskFunding: one.finalDataObj.taskFunding,
                            selectedStatus: one.finalDataObj.selectionStatus,
                            proposalStatus: one.finalDataObj.proposalStatus,
                            taskTitle: one.finalDataObj.taskTitle
                        }
                        allBids.push(bidInfo);
                    }
                })
            }
        })
        return res.json({tasks: allBids})

    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/bids/from', async (req, res) => {
    let allBids = []
    try {
        let getTasks = await taskModel.find({
            'hive.author': req.body.username
        });

        await getTasks.forEach(async one => {
            if (one.bids.length > 0) {
                await one.bids.forEach(bid => {
                    let bidInfo =  {
                        hive: bid.hive,
                        otherData: bid.otherData,
                        votes: bid.votes,
                        paymentMode: one.finalDataObj.paymentMode,taskFunding: one.finalDataObj.taskFunding,
                        selectedStatus: one.finalDataObj.selectionStatus,
                        proposalStatus: one.finalDataObj.proposalStatus,
                        taskTitle: one.finalDataObj.taskTitle
                    }
                    allBids.push(bidInfo);
                })
            }
        })
        return res.json({tasks: allBids})

    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/edit', async (req, res) => {
    const body = req.body;

    const hive = req.body.comment
    const finalDataObj = req.body.finalDataObj

    try {

        console.log(hive.author)

        const getTask = await taskModel.findOne({'hive.author': hive.author, 
        'hive.permlink': hive.permlink})
        
        const saveTask = await taskModel.findByIdAndUpdate(getTask._id, {
            hive,
            finalDataObj,
            bids: getTask.bids
        })

        return res.json({message: 'success'})
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

router.post('/delete', async (req, res) => {
    const body = req.body;

    const author = req.body.author
    const permlink = req.body.permlink

    try {

        const getTask = await taskModel.findOne({
            'hive.author': author, 
            'hive.permlink': permlink
        })
        
        await taskModel.findByIdAndDelete(getTask._id);

        return res.json({message: 'success'});
    } catch (error) {
        return res.json({
            message: error.message
        })
    }
})

module.exports = router;

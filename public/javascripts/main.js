$(document).ready(async () => {
    $(document).on('click', '.navbar-toggler-icon', async () => {

        if ($('#navbarSupportedContent').css('display') !== 'none') {
            console.log('closing')
            $('#navbarSupportedContent').css('display', 'none')
        }

        else if ($('#navbarSupportedContent').css('display') === 'none') {
            console.log('opening')
            $('#navbarSupportedContent').css('display', 'inline-block')
        }
    })
})
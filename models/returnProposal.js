const mongoose = require('mongoose')
const Schema  = mongoose.Schema;

const returnProposalSchema = new Schema({
    setBy: String,
    setValue: Number,
    date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('ReturnProposal', returnProposalSchema);
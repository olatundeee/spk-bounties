const mongoose = require('mongoose')
const Schema  = mongoose.Schema;

const taskSchema = new Schema({
    hive: {
        parent_author:  String,
        parent_permlink: String,
        author: String,
        permlink: String,
        title: String,
        body:   String,
        json_metadata: String
    },
    finalDataObj: {
        taskTitle: String,
        taskBidAccept: Number,
        taskMilestoneStatus: String,
        taskFunding: String,
        taskDesc: String,
        milestones: Array,
        milestonesFinal: Array,
        lineItems: Array,
        lineItemsFinal: Array,
        returnProposal: String,
        paymentMode: String,
        taskDuration: String,
        selectionStatus: {type: Boolean, default: false},
        proposalStatus: {type: String, default: 'Receiving votes'}
    },
    votes:[
        {
            voter: String,
            voteType: String,
            voterHivePower: String,
            voteReceiver: String,
            permlink: String
        }
    ],
    proposalData: {},
    bids: [
        {
            hive: {
                parent_author:  String,
                parent_permlink: String,
                author: String,
                permlink: String,
                title: String,
                body:   String,
                json_metadata: String
            },
            otherData: {
                priceLump: String,
                priceMilestone: Array,
                taskPermlink: String,
                bidComment: String,
                bidPrevExp: String,
                bidStatus: {type: Boolean, default: false},
                selected: {type: Boolean, default: false}
            },
            
            votes: [
                {
                    voter: String,
                    voteType: String,
                    voterHivePower: String,
                    voteTaskAuthor: String,
                    voteTaskPermlink: String,
                    voteBidAuthor: String,
                    voteBidPermlink: String,
                    voteBidComment: String
                }
            ]
        }
    ],
    date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Task', taskSchema);
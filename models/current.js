const mongoose = require('mongoose')
const Schema  = mongoose.Schema;

const currentSchema = new Schema({
    author: String,
    finalDataObj: {
        taskTitle: String,
        taskBidAccept: Number,
        taskMilestoneStatus: String,
        taskFunding: String,
        taskDesc: String,
        milestones: Array,
        milestonesFinal: Array,
        lineItems: Array,
        lineItemsFinal: Array,
        returnProposal: String,
        paymentMode: String,
        taskDuration: String
    },
    date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Current', currentSchema);